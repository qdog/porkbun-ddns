dynamic dns for porkbun
# Installation
```sh
cp porkbun-ddns /usr/local/bin/
```

# Config
your config file should look something like this
```json
{
  "endpoint": "https://api-ipv4.porkbun.com/api/json/v3",
  "domain": "mydomain.com",
  "subdomain": "ddns",
  "api_auth": {
    "apikey": "pk1_....",
    "secretapikey": "sk1_..."
  }
}

```
and put it in `/etc/porkbun-ddns/config.json`


# Systemd
to run this script regularly, create a systemd service
in your local user systemd config
`~/.config/systemd/user/porkbun-ddns.service`
```systemd
[Unit]
Description=Update A records for porkbun DNS

[Service]
ExecStart=/usr/local/bin/porkbun-ddns

[Install]
WantedBy=default.target
```

and create another timer service that will run every 10 minutes
`~/.config/systemd/user/porkbun-ddns.timer`
```systemd
[Unit]
Description=Run porkbun-dns every 5 minute

[Timer]
OnBootSec=10min
OnCalendar=*:0/5

[Install]
WantedBy=timers.target
```

then
```sh
systemctl --user enable porkbun-ddns.timer
systemctl --user start porkbun-ddns.timer
```

# Logs
you can monitor the logs by 
```sh
journalctl --user -u porkbun-dns
```

